package com.example.przychodnia.dao;

import com.example.przychodnia.model.Doctor;
import org.springframework.data.repository.CrudRepository;

public interface DoctorRepository extends CrudRepository<Doctor, Long> {
}
