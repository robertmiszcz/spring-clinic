package com.example.przychodnia.dao;

import com.example.przychodnia.model.Patient;
import org.springframework.data.repository.CrudRepository;



public interface PatientRepository extends CrudRepository<Patient, Long>{
}
