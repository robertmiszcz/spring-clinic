package com.example.przychodnia.controller;

import com.example.przychodnia.dao.DoctorRepository;
import com.example.przychodnia.model.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DoctorController {

    @Autowired
    private DoctorRepository doctorRepository;

    @GetMapping("/add")
    public String addDoctor(ModelMap modelMap) {
        doctorRepository.save(new Doctor("Andrzej Falkowicz", "chirurg"));
        doctorRepository.save(new Doctor("Marek Czapla", "psychiatra"));
        doctorRepository.save(new Doctor("Magdalena Dzika", "pediatra"));
        modelMap.put("doctors", doctorRepository.findAll());
        return "/add";
    }
}
