package com.example.przychodnia.controller;


import com.example.przychodnia.dao.DoctorRepository;
import com.example.przychodnia.dao.PatientRepository;

import com.example.przychodnia.model.Doctor;
import com.example.przychodnia.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;

@Controller
public class PatientController {

    @Autowired
    private PatientRepository patientRepository;



    @GetMapping("/index")
    public String index(){
        return "/index";
    }
//





    @PostMapping("/create")
    public String create(@ModelAttribute Patient patient, ModelMap modelMap) {
        patientRepository.save(patient);
        modelMap.addAttribute("patient", patient);
        return "create";
    }
}