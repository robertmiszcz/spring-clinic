package com.example.przychodnia.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Doctor {

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "doctor")
    private List<Patient> patients;

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String specialization;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Doctor() {

    }

    public Doctor(String name) {

        this.name = name;
    }

    public Doctor(String name, String specialization) {

        this.name = name;
        this.specialization = specialization;
    }

    @Override
    public String toString() {
        return "dr " +name + ", specialność " + specialization + ' ' ;
    }
}
